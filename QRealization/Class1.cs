﻿using System;

namespace QRealization
{
    public class Class1
    {
        public double[] solve(double a, double b, double c) {
            const double e = 1e-5;
            if(Math.Abs(a) <= e)
            {
                throw new ArgumentException("coefficient a=0");
            }
            double d2 = b*b - 4*a*c;
            if(d2 >= e )
            {
                double d = Math.Sqrt(d2);
                return new double[] {(-b - d)/(2*a),(-b + d)/(2*a)};
            }
            else { 
                if (Math.Abs(d2) < e) {
                return new double[] {-b/(2*a)};
                }
            }
            return new double[0];
            
        }
    }
}
