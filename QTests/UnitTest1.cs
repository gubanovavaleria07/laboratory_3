using System;
using QRealization;
using Xunit;

internal class MyAssert {
    public static void Equal(Double expected, Double actual, Double eps) {
        if (Math.Abs(expected - actual) > eps) {
            throw new DoubleExeption(expected: expected.ToString(), actual: actual.ToString(), message: "");
        }
    }    
}

internal class DoubleExeption: Xunit.Sdk.AssertActualExpectedException {
    internal DoubleExeption(string expected, string actual, string message) 
    : base(expected, actual, message) {}
}

namespace QTests
{
    public class UnitTest1
    {
        [Fact]
        public void QTest_1_Minus2_1_MustHaveOneRoot()
        {
             Class1 c = new Class1();
             double[] result = c.solve(1,-2,1);
             MyAssert.Equal(1, result[0], 1e-5);
        }

        [Fact]
        public void QTest_1_0_Minus1_MustHaveTwoRoots()
        {
            Class1 c = new Class1();
            double[] result = c.solve(1,0,-1);
            Array.Sort(result);
            MyAssert.Equal(-1, result[0], 1e-5);
            MyAssert.Equal(1, result[1], 1e-5);
        }

        [Fact]
        public void QTest_1_0_1_NoHaveRoots()
        {
            Class1 c = new Class1();
            double[] result = c.solve(1,0,1);
            MyAssert.Equal(0, result.Length, 1e-5);
        }

         [Fact]
        public void QTestCoefficientAMustNotBeZero()
        {
            Class1 c = new Class1();
            Assert.Throws<ArgumentException>(() => c.solve(1e-5,1,1));
        }
    }
}
